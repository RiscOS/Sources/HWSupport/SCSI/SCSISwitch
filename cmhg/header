; Copyright 2003 Tematic Ltd
;
; Licensed under the Apache License, Version 2.0 (the "License");
; you may not use this file except in compliance with the License.
; You may obtain a copy of the License at
;
;     http://www.apache.org/licenses/LICENSE-2.0
;
; Unless required by applicable law or agreed to in writing, software
; distributed under the License is distributed on an "AS IS" BASIS,
; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
; See the License for the specific language governing permissions and
; limitations under the License.
;
#include "VersionNum"

#include "Global/Services.h"

title-string:           SCSIDriver
help-string:            SCSIDriver Module_MajorVersion_CMHG Module_MinorVersion_CMHG
date-string:            Module_Date_CMHG

finalisation-code:      module_finalise
initialisation-code:    module_initialise

service-call-handler:   module_service_handler Service_ResourceFSStarting

international-help-file: "Resources:$.Resources.SCSIDriver.Messages"

swi-chunk-base-number:  &403C0

swi-handler-code:       module_swi_handler

swi-decoding-table:     SCSI, Version, Initialise, Control, Op, Status,
                        5, 6, Reserve, List, 9, 10, 11, 12, 13, 14,
                        15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26,
                        27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38,
                        39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50,
                        51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61,
                        Deregister,
                        Register

#ifdef NO_INTERNATIONAL_HELP

command-keyword-table:  module_cmd_handler
                        SCSIDevices(min-args:0, max-args:0, add-syntax:,
                            help-text: "*SCSIDevices lists type, capacity and vendor details of "
                                       "attached SCSI devices\n",
                            invalid-syntax: "Syntax: *SCSIDevices")
#else

command-keyword-table:  module_cmd_handler
                        SCSIDevices(min-args:0, max-args:0, international:,
                            help-text: "HDV",
                            invalid-syntax: "SDV")

#endif

generic-veneers:        driver_callback_entry/driver_callback_handler,
                        driver_error_entry/driver_error_handler
